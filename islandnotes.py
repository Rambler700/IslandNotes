# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 20:47:19 2017

@author: enovi
"""
##License: GPL 3.0

##Island Notes

#import random
import msvcrt
import winsound
game_loop  = True
coconuts   = 0
torches    = 0
ropes      = 0
treasure1  = False
planks     = 0
diamonds   = 0
branches   = 0
treasure2  = False
spoon      = 0
fork       = 0
knife      = 0
treasure3  = False
face_mask  = 0
waterskin  = 0
hammer     = 0
treasure4  = False
conchshell = 0
clamshell  = 0
snailshell = 0
treasure5  = False

def main():
    choice = 'y'
    print("***Welcome to Island Notes!***")
    print('Play songs to solve puzzles and gain quest items!')
    print('Use quest items to gain treasures!')
    while choice == 'y':
        choice = input('New game y/n? ').lower()
        if choice == 'y':
            print ('Starting new game')
            set_defaults()
            island_intro()
            island_notes()
        elif choice == 'n':
            return
        else:
            print('Enter a valid choice')
            choice = 'y'

def set_defaults():
    global game_loop
    global coconuts
    global torches
    global ropes
    global treasure1
    global planks
    global diamonds
    global branches
    global treasure2
    global spoon
    global fork
    global knife
    global treasure3
    global face_mask
    global waterskin
    global hammer
    global treasure4
    global conchshell
    global clamshell
    global snailshell
    global treasure5
    game_loop = True
    coconuts  = 0
    torches   = 0
    ropes     = 0
    treasure1 = False
    planks    = 0
    diamonds  = 0
    branches  = 0
    treasure2 = False
    spoon     = 0
    fork      = 0
    knife     = 0
    treasure3 = False
    face_mask = 0
    waterskin = 0
    hammer    = 0
    treasure4 = False
    conchshell = 0
    clamshell  = 0
    snailshell = 0
    treasure5  = False

#electronic keyboard
def soundtest():
    key = ''
    played_notes = []
    play_sound = True
    print('Electronic keyboard')
    print('Keyboard includes keys a-l and z-c')
    print('Press q to end song')
    print('Octave is 3')
    while play_sound == True:
        if msvcrt.kbhit():
            key = str(msvcrt.getch())
            #print(key)
            if key == "b'a'":
                print('C')
                winsound.Beep(131,400)
                played_notes.append('C')
            if key == "b's'":
                print('C#')
                winsound.Beep(139,400)
                played_notes.append('C#')
            if key == "b'd'":
                print('D')
                winsound.Beep(147,400)
                played_notes.append('D')
            if key == "b'f'":
                print('D#')
                winsound.Beep(156,400)
                played_notes.append('D#')
            if key == "b'g'":
                print('E')
                winsound.Beep(165,400)
                played_notes.append('E')
            if key == "b'h'":
                print('F')
                winsound.Beep(175,400)
                played_notes.append('F')
            if key == "b'j'":
                print('F#')
                winsound.Beep(185,400)
                played_notes.append('F#')
            if key == "b'k'":
                print('G')
                winsound.Beep(196,400)
                played_notes.append('G')
            if key == "b'l'":
                print('G#')
                winsound.Beep(208,400)
                played_notes.append('G#')
            if key == "b'z'":
                print('A')
                winsound.Beep(220,400)
                played_notes.append('A')
            if key == "b'x'":
                print('A#')
                winsound.Beep(233,400)
                played_notes.append('A#')
            if key == "b'c'":
                print('B')
                winsound.Beep(247,400)
                played_notes.append('B')
            #exit command
            if key == "b'q'":
                print('Ending song')
                play_sound = False
    return played_notes    
    
def island_intro(): 
    print('You are sitting on a sandy beach.')
    print('You see pieces of wood scattered around the shore.')
    print('Nobody else is nearby.')
    print('The only object you see is a battered guitar.')
    print('You pick up the guitar.')
    
def palm_trees():
    global coconuts
    notes = []
    print('A group of birds are sitting in the trees, singing a song.') 
    winsound.Beep(220,400)
    winsound.Beep(233,400)
    winsound.Beep(247,400)
    winsound.Beep(220,400)
    winsound.Beep(233,400)
    winsound.Beep(247,400)
    winsound.Beep(220,400)
    winsound.Beep(233,400)
    winsound.Beep(247,400)
    winsound.Beep(220,400)
    winsound.Beep(233,400)
    winsound.Beep(247,400)
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['A','A#','B','A','A#','B','A','A#','B','A','A#','B']:
        print('The birds seem impressed.')
        print('A bird drops a fresh coconut at your feet.')
        coconuts += 1
    else:
        print('The birds seem uninterested in your song.')
        
def rocks():
    global torches
    notes = []
    print('Birds are sitting on the rocks, singing another song.')
    winsound.Beep(185,400)
    winsound.Beep(196,400)
    winsound.Beep(185,400)
    winsound.Beep(196,400)
    winsound.Beep(185,400)
    winsound.Beep(196,400)
    winsound.Beep(185,400)
    winsound.Beep(196,400)
    winsound.Beep(185,400)
    winsound.Beep(196,400)
    winsound.Beep(185,400)
    winsound.Beep(196,400)
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['F#','G','F#','G','F#','G','F#','G','F#','G','F#','G']:
        print('The birds chirp along with the music.')
        print('A bird drops an unlit torch in front of you.')
        torches += 1
    else:
        print('The birds seem uninterested in your song.')
        
def oasis():
    global ropes
    notes = []
    print('Birds are drinking water from the oasis.')
    print('Suddenly, they begin chirping a song.')
    winsound.Beep(139,400)
    winsound.Beep(147,400)
    winsound.Beep(156,400)
    winsound.Beep(156,400)
    winsound.Beep(147,400)
    winsound.Beep(139,400)
    winsound.Beep(139,400)
    winsound.Beep(147,400)
    winsound.Beep(156,400)
    winsound.Beep(156,400)
    winsound.Beep(147,400)
    winsound.Beep(139,400)
    notes = soundtest()
    if notes == ['C#','D','D#','D#','D','C#','C#','D','D#','D#','D','C#']:
        print('The birds flap around excitedly.')
        print('A bird soars into the air, and you see a rope where the bird was standing.')
        ropes += 1
    else:
        print('The birds continue drinking water.')
        
def cave():
    global coconuts
    global torches
    global ropes
    global treasure1
    advanced = False
    print('You enter the cave.')
    print('You suddenly feel thirsty.')
    if coconuts > 0:
        print('You drink some coconut milk.')
        coconuts -= 1
        advanced = True
        ##
        print('The cave is becoming very dark.')    
        if torches > 0 and advanced == True:
            print('You light the torch and walk further into the cave.')
            torches -= 1
            advanced = True
            ##
            print('You see a towering rock formation.')    
            if ropes > 0 and advanced == True:
                if treasure1 == False:
                    print('You use the rope to scale the rocks and find a chest of treasure.')
                    ropes -= 1
                    treasure1 = True
                else:
                    print('You search the area, but do not find any more treasure.')
            else:
                print('You cannot climb the rocks.')
                advanced = False         
        else:
            print('You cannot see the path and decide to exit the cave.')
            advanced = False
    else:
        print('You leave the cave in search of a drink.')
        advanced = False    

def shipwreck():
    global planks
    notes = []
    print('You see crates, planks, and boxes scattered around the beach.')
    print('Suddenly a ghost appears and sings a haunting song.')
    winsound.Beep(175,400)
    winsound.Beep(165,400)
    winsound.Beep(175,400)
    winsound.Beep(165,400)
    winsound.Beep(175,400)
    winsound.Beep(165,400)
    winsound.Beep(175,400)
    winsound.Beep(165,400)
    winsound.Beep(175,400)
    winsound.Beep(165,400)
    winsound.Beep(175,400)
    winsound.Beep(165,400)
    print('As the song fades away, a second ghost begins to sing.')
    winsound.Beep(156,400)
    winsound.Beep(165,400)
    winsound.Beep(156,400)
    winsound.Beep(165,400)
    winsound.Beep(156,400)
    winsound.Beep(165,400)
    winsound.Beep(156,400)
    winsound.Beep(165,400)
    winsound.Beep(156,400)
    winsound.Beep(165,400)
    winsound.Beep(156,400)
    winsound.Beep(165,400)
    print('The ghosts look around, as if searching for another band member.')
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['D','D#','D','D#','D','D#','D','D#','D','D#','D','D#']:
        print('Suddenly, another ghost appears and sings along with the other two.')
        print('The song ends, and the ghosts fade away. You see a sturdy plank on the ground.')
        planks += 1
    else:
        print('The ghosts continue to search for their missing friend.') 
        
def cliff():
    global diamonds
    notes = []        
    print('You see a cliff. A flock of birds is sitting on the cliff.')
    print('Another flock of birds is circling in the air.')
    print('The flock of birds sitting on the cliff begins to sing a song.')
    winsound.Beep(196,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)
    winsound.Beep(196,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)
    winsound.Beep(196,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)
    winsound.Beep(196,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)
    print('The birds in the air respond with another song.')
    winsound.Beep(208,400)
    winsound.Beep(220,400)   
    winsound.Beep(233,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)   
    winsound.Beep(233,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)   
    winsound.Beep(233,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)   
    winsound.Beep(233,400)
    print('The birds sitting on the cliff fly away.')
    print('The birds in the air take their place.')
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['A','A#','B','A','A#','B','A','A#','B','A','A#','B']:
        print('The second flock of birds flies away, leaving a diamond behind.')
        diamonds += 1
    else:
        print('The birds perch on the cliff contentedly.')
        
def forest():
    global branches
    notes = []
    print('You enter a dark forest.')
    print('Suddenly you hear wolves howling in the distance.')
    winsound.Beep(220,400)
    winsound.Beep(220,400)  
    winsound.Beep(196,400)
    winsound.Beep(196,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)  
    winsound.Beep(196,400)
    winsound.Beep(196,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)  
    winsound.Beep(196,400)
    winsound.Beep(196,400)
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['A','A','G','G','A','A','G','G','A','A','G','G']:
        print('The howls fade, and you continue to explore the forest.')
        print('You pick up a tree branch that looks like it could be useful for exploring.')
        branches += 1
    else:
        print('The howls grow louder, and seem closer.')
        print('You quickly leave the forest.')  
        
def temple():
    global planks
    global diamonds
    global branches
    global treasure2
    advanced = False
    print('You enter the temple.')
    print('It appears that nobody has been inside this temple for many years.')
    print('You enter the main hall. There is a large hole in the floor.')
    if planks > 0:
        print('You use the plank to bridge the gap and continue onward.')
        planks -= 1
        advanced = True
        ##
        print('You see a statue with an empty eye socket. There is a diamond in the other socket.')    
        if diamonds > 0 and advanced == True:
            print('You place the diamond in the socket.')
            print('A wall panel recedes, revealing a hidden room.')
            diamonds -= 1
            advanced = True
            ##
            print('You see a treasure chest sitting on a high platform and a distant switch.')    
            if branches > 0 and advanced == True:
                if treasure2 == False:
                    print('You flip the switch using a branch.')
                    print('The platform descends to the floor, along with the treasure chest.')
                    branches -= 1
                    treasure2 = True
                else:
                    print('You have already obtained this treasure.')
            else:
                print('You cannot reach the switch and decide to leave the temple.')
                advanced = False
        else:
            print('You cannot remove the diamond and see no further paths for exploration.')
            advanced = False    
    else:
        print('You cannot jump across the gap, and decide to leave the temple.')
        advanced = False
        
def monument():
    global spoon
    notes = []
    print('You see a stone monument up ahead.')
    print('There is an inscription on the monument. It says:')
    print('Birds And Beasts Fear Grinning Ghosts But Fire And Flame Bring Calmness.')
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['B','A','B','F','G','G','B','F','A','F','B','C']:
        print('A hidden compartment opens up near the base of the monument.')
        print('You find a metal spoon inside the chamber.')
        spoon += 1
    else:
        print('The stone monument remains unchanged.')
        
def graveyard():
    global fork
    notes = []
    print('The gravestones here are covered in weeds and undergrowth.')
    print('You hear a bell ringing.')
    winsound.Beep(139,400)   
    winsound.Beep(139,400)
    winsound.Beep(156,400)
    winsound.Beep(156,400)
    winsound.Beep(147,400)
    winsound.Beep(147,400)
    print('The bell stops abruptly, as if a song was interrupted in the middle.')
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['C#','C#','D#','D#','D','D','C#','C#','D#','D#','D','D']:
        print('The bell begins to ring again.')
        winsound.Beep(139,400)   
        winsound.Beep(139,400)
        winsound.Beep(156,400)
        winsound.Beep(156,400)
        winsound.Beep(147,400)
        winsound.Beep(147,400)
        print('You hear another ringing sound as a metal object falls to the ground.')
        print('A fork is on the floor underneath the bell. You pick it up.')
        fork += 1
    else:
        print('The bell remains silent.')  

def stream():
    global knife
    notes = []        
    print('You see a rapidly flowing stream in front of you.')
    print('The water is flowing toward the shore.') 
    print('You look toward the shore and see a shiny object underneath the water.')
    print('Frogs sit on the banks of the stream. They croak a melody.')
    print('You take out your guitar.')
    winsound.Beep(131,400)
    winsound.Beep(139,400)
    winsound.Beep(147,400)
    winsound.Beep(156,400)
    winsound.Beep(165,400)
    winsound.Beep(175,400)
    winsound.Beep(185,400)
    winsound.Beep(196,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)
    winsound.Beep(233,400)
    winsound.Beep(247,400)    
    notes = soundtest()
    if notes == ['B','A#','A','G#','G','F#','F','E','D#','D','C#','C']:
        print('The frogs suddenly stop croaking.')
        print('The water begins flowing inland.')
        print('The shiny object is dislodged from the floor of the stream.')
        print('The current drops a shiny knife at your feet.')
        knife += 1
    else:
        print('The frogs keep croaking, and the water keeps flowing toward the shore.')    
    
def canyon():
    global spoon
    global knife
    global fork
    global treasure3
    print('You enter a trail heading into the canyon.')
    print('You see an abandoned shack on the side of the trail.')
    print('You enter the shack and see a table.')
    print('You see a plate on the table, but no utensils.')
    if treasure3 == True:
        print('You have already obtained this treasure.')
    else:
        if spoon > 0 and knife > 0 and fork > 0:
            print('You place a spoon, fork, and knife on the table.')
            print('You hear a sudden thump outside the shack.')
            print('Outside the door, you find a treasure chest.')
            treasure3 = True
            print('You look back inside the shack, and the utensils are gone.')
            spoon -= 1
            knife -= 1
            fork  -= 1
        else:
            print('You feel like this table setting is still missing some utensils.')  
            
def prairie():
    global face_mask
    notes = [] 
    print('In the middle of the island, you see a vast prairie.')
    print('You continue walking across the prairie.')
    print('Suddenly, a prairie dog pops up from a hole in the ground.') 
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    winsound.Beep(247,400)
    winsound.Beep(247,400)
    winsound.Beep(233,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    winsound.Beep(247,400)
    winsound.Beep(247,400)
    winsound.Beep(233,400)
    print('Another prairie dog appears and joins in.')
    winsound.Beep(185,400)
    winsound.Beep(185,400)
    winsound.Beep(185,400)
    winsound.Beep(208,400)
    winsound.Beep(208,400)
    winsound.Beep(196,400)
    winsound.Beep(185,400)
    winsound.Beep(185,400)
    winsound.Beep(185,400)
    winsound.Beep(208,400)
    winsound.Beep(208,400)
    winsound.Beep(196,400)
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['D#','D#','D#','F','F','E','D#','D#','D#','F','F','E']:
        print('The prairie dogs jump out of their holes.')
        print('You see a face mask lying on the ground.')
        face_mask += 1
    else:
        print('The prairie dogs continue to sing to each other.') 

def waterfall():
    global waterskin
    notes = []
    print('You continue walking, and arrive at a loud waterfall.')
    print('Birds are perched on the nearby rocks.')
    print('You see a rusty switch flipped to the downward position.')
    print('The switch is too far away to reach.')
    print('The birds begin singing a song.')
    winsound.Beep(233,400)
    winsound.Beep(220,400)
    winsound.Beep(208,400)
    winsound.Beep(196,400)
    winsound.Beep(233,400)
    winsound.Beep(220,400)
    winsound.Beep(208,400)
    winsound.Beep(196,400)
    winsound.Beep(233,400)
    winsound.Beep(220,400)
    winsound.Beep(208,400)
    winsound.Beep(196,400)
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['G','G#','A','A#','G','G#','A','A#','G','G#','A','A#']:
         print('The switch flips to an upright position.')
         print('A waterskin drops from a hidden compartment and rolls to your feet.')
         print('You fill the waterskin with water.')
         waterskin += 1
    else:
        print('The birds keep singing and the switch stays in the downward position.')

def orchard():
    global hammer
    notes = []
    print('You enter an apple orchard and see birds singing once again.')
    print('There is a locked shed between the apple trees.')
    winsound.Beep(233,400)
    winsound.Beep(233,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    winsound.Beep(233,400)
    winsound.Beep(233,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    winsound.Beep(208,400)
    winsound.Beep(208,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['A#','A#','A','A','A#','A#','A','A','G#','G#','A','A']:
        print('The song echoes in the orchard for a few seconds.')
        print('An apple tree crashes into the shed and knocks down the door.')
        print('You search the shed and find a hammer.')
        hammer += 1
    else:    
        print('The shed remains locked and the birds keep singing.')
        
def desert():
    global face_mask
    global waterskin
    global hammer
    global treasure4
    advanced = False
    print('You enter a vast and windy desert.')
    print('Suddenly, the wind begins blowing fiercely.')
    print('The wind begins blowing sand in your face.')
    if face_mask > 0:
        print('You put on your face mask and continue onward.')
        face_mask -= 1
        advanced = True
        ##
        print('The desert is very hot, and you become thirsty.')    
        if waterskin > 0 and advanced == True:
            print('You drink the water from the waterskin.')
            waterskin -= 1
            advanced = True
            ##
            print('You see a shrine with a stone door.')
            print('You push on the door, but it seems to be stuck.')
            if hammer > 0 and advanced == True:
                if treasure4 == False:
                    print('You break the door down with your hammer.')
                    print('Inside the shrine, you find a treasure chest.')
                    hammer -= 1
                    treasure4 = True
                else:
                    print('You have already obtained this treasure.')
            else:
                print('You cannot reach the switch and decide to leave the temple.')
                advanced = False
        else:
            print('You leave the desert to search for a source of water.')
            advanced = False    
    else:
        print('The sandstorm prevents you from continuing any further.')
        advanced = False   
        
def tidepool():
    global conchshell
    notes = []
    print('You see a tide pool. It is full of crabs.')
    print('The crabs play a song with their claws.')
    winsound.Beep(196,400)
    winsound.Beep(196,400)
    winsound.Beep(233,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    winsound.Beep(185,400)
    winsound.Beep(196,400)
    winsound.Beep(196,400)
    winsound.Beep(233,400)
    winsound.Beep(220,400)
    winsound.Beep(220,400)
    winsound.Beep(185,400)
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['G','G','A#','A','A','F#','G','G','A#','A','A','F#']:
        print('The crabs scuttle away.')
        print('You see a conch shell that the crabs left behind.')
        conchshell += 1
    else:    
        print('The crabs keep clicking their claws.') 
        
def pier():
    global clamshell
    notes = []
    print('You walk out onto an abandoned pier.')      
    print('The waves crash loudly against the pillars of the pier.')
    print('Gulls are singing a song, but it is hard to hear above the waves.')
    print('You hear a few notes, and then the song stops.')
    winsound.Beep(220,400)
    winsound.Beep(175,400)
    winsound.Beep(220,400)
    winsound.Beep(175,400)
    winsound.Beep(185,400)
    winsound.Beep(185,400)
    print('The song suddenly starts up again.')
    winsound.Beep(220,400)
    winsound.Beep(175,400)
    winsound.Beep(220,400)
    winsound.Beep(175,400) 
    winsound.Beep(196,400)
    winsound.Beep(196,400)
    print('You take out your guitar.')
    notes = soundtest()    
    if notes == ['A','F','A','F','F#','F#','A','F','A','F','G','G']:
        print('The gulls seem impressed.')
        print('A gull drops a clam shell at your feet.')
        clamshell += 1
    else:
        print('The gulls continue to squawk at each other.')
        
def lagoon():
    global snailshell
    notes = []
    print('The water in the lagoon is crystal clear.')
    print('There is a plaque on a nearby rock.')
    print('It says Four Fresh Bird Eggs.')
    print('You do not see any bird eggs.')
    print('You take out your guitar.')
    notes = soundtest()
    if notes == ['F','B','E','F','B','E','F','B','E','F','B','E']:
        print('The waters in the lagoon grow cloudy for a few seconds.')
        print('A snail shell flies out of the water and lands in front of you.')
        snailshell += 1
    else:
        print('The lagoon remains undisturbed.')
        
def shrine():
    global knife
    global planks
    global branches
    advanced = False   
    choice = ''
    print('You see a shrine on a hill near the shore.')
    print('You enter the shrine and see a sign.')
    choice = input('Read the sign y/n? ')
    if choice == 'y':
        print('Travelers may need to retrace their steps before venturing forth.')
        print('You may see frogs, ghosts, and wolves again.')
        print('You enter the shrine. There is a pit in the floor, preventing you from continuing.')
        if planks > 0:
            print('You place the plank above the pit and continue onward.')
            planks -= 1
            advanced = True
            ##
            print('You see a lever on the ceiling.')    
            if branches > 0 and advanced == True:
                print('You use the branch to flip the lever.')
                branches -= 1
                advanced = True
                ##
                print('You see a metal door, covered in thick vines.')
                if knife > 0 and advanced == True:
                    print('You use the knife to clear away the vines, revealing a button.')
                    print('You push a button and the door opens.')
                    print('You enter the inner shrine.')
                    inner_shrine()
                else:
                    print('You cannot remove the vines so you leave the temple.')
                    advanced = False
            else:
                print('You cannot reach the lever so you leave the temple.')
                advanced = False    
        else:
            print('You cannot cross the pit so you leave the temple.')
            advanced = False   
            
def inner_shrine():
    global conchshell
    global clamshell
    global snailshell
    global treasure5
    advanced = False
    print('There is a symbol of a conch on the wall, and a shelf underneath.')
    if conchshell > 0:
        print('You place the conch shell on the shelf and a door opens.')
        conchshell -= 1
        advanced = True
        ##
        print('You see a clam painted on the wall, and another shelf.')    
        if clamshell > 0 and advanced == True:
            print('You place the clam shell on the shelf and a second door opens.')
            clamshell -= 1
            advanced = True
            ##
            print('You see a statue of a snail, and another shelf underneath it.')
            if snailshell > 0 and advanced == True:
                if treasure5 == False:
                    print('You place the snail shell on the shelf and a third door opens.')
                    print('You walk through the door and find a treasure chest.')
                    snailshell -= 1
                    treasure5 = True
                else:
                    print('You have already obtained this treasure.')
            else:
                print('You need a snail shell to continue further.')
                advanced = False
        else:
            print('You need a clam shell to continue further.')
            advanced = False    
    else:
        print('You need a conch shell to continue further.')
        advanced = False               
            
def inner_island():
    choice6 = ''
    choice6 = input('Visit the (p)rairie, the (w)aterfall, the (o)rchard, the (d)esert, the (f)ar shore, or return to (b)ase: ')
    if choice6 == 'p':
        prairie()
    if choice6 == 'w':
        waterfall()
    if choice6 == 'o':
        orchard()    
    if choice6 == 'd':    
        desert()
    if choice6 == 'f':
        print('You continue walking to the far shore.')
        farshore()
    if choice6 == 'b':
        print('You return to your camp.')

def farshore():
    choice6 = ''
    choice6 = input('Visit the (t)idepool, the (p)ier, the (l)agoon, the (s)hrine or return to (b)ase: ')
    if choice6 == 't':
        tidepool()
    if choice6 == 'p':
        pier()
    if choice6 == 'l':
        lagoon()    
    if choice6 == 's':    
        shrine()
    if choice6 == 'b':
        print('You return to your camp.')
    
def inventory():
    global coconuts
    global torches
    global ropes
    global treasure1
    global planks
    global diamonds
    global branches
    global treasure2
    global spoon
    global fork
    global knife
    global treasure3
    global face_mask
    global waterskin
    global hammer
    global treasure4
    global conchshell
    global clamshell
    global snailshell
    global treasure5
    print('You have {} coconuts'.format(coconuts))
    print('You have {} torches'.format(torches))
    print('You have {} ropes'.format(ropes))
    if treasure1 == True:
        print('You have the first treasure.')
    print('You have {} planks'.format(planks))
    print('You have {} diamonds'.format(diamonds))
    print('You have {} branches'.format(branches))
    if treasure2 == True:
        print('You have the second treasure.') 
    print('You have {} spoons'.format(spoon))
    print('You have {} forks'.format(fork))
    print('You have {} knives'.format(knife))
    if treasure3 == True:
        print('You have the third treasure.')  
    print('You have {} face masks'.format(face_mask))
    print('You have {} waterskins'.format(waterskin))
    print('You have {} hammers'.format(hammer))
    if treasure4 == True:
        print('You have the fourth treasure.')   
    print('You have {} conch shells'.format(conchshell))
    print('You have {} clam shells'.format(clamshell))
    print('You have {} snail shells'.format(snailshell))
    if treasure5 == True:
        print('You have the fifth treasure.')    
        
def island_notes():
    global game_loop
    choice1 = ''
    choice2 = ''
    choice3 = ''
    choice4 = ''
    choice5 = ''
    choice6 = ''
    notes = []
    while game_loop == True:
        if treasure1 == True and treasure2 == True and treasure3 == True and treasure4 == True and treasure5 == True:
            print('You have discovered all of the treasures on the island!')
            print('You construct a boat and sail away with your treasures.')
            game_loop = False
            return
        print('A new day has begun.')
        choice1 = input('Check inventory y/n? ')
        if choice1 == 'y':
            inventory()        
        choice2 = input('Continue? Enter y to continue or q to quit: ')    
        if choice2 == 'q':
            game_loop = False
            return
        else:
            print('You pick up your guitar.')
        choice3 = input('Play a song? Enter y or n: ')
        if choice3 == 'y':
            notes = soundtest()
            print('You played these notes: ',notes)
        choice4 = input('Explore the island? Enter y or n: ')    
        if choice4 == 'y':
            print('You stand up and look around.')
            choice5 = input('You can walk (d)own the beach, (u)p the beach, or (i)nland: ')
            if choice5 == 'd':
                print('You walk down the beach.')
                choice6 = input('Visit the (p)alm trees, (r)ocks, (o)asis, (c)ave, or return to (b)ase: ')
                if choice6 == 'p':
                    palm_trees()
                if choice6 == 'r':
                    rocks()
                if choice6 == 'o':
                    oasis()
                if choice6 == 'c':
                    cave()
                if choice6 == 'b':
                    print('You return to your camp.')
            if choice5 == 'u':
                print('You walk up the beach.')
                choice6 = input('Visit the (s)hipwreck, the (c)liff, the (f)orest, the (t)emple, or return to (b)ase: ')
                if choice6 == 's':
                    shipwreck()
                if choice6 == 'c':
                    cliff()
                if choice6 == 'f':
                    forest()
                if choice6 == 't':    
                    temple()
                if choice6 == 'b':
                    print('You return to your camp.')
            if choice5 == 'i':
                print('You walk inland.')
                choice6 = input('Visit the (m)onument, the (g)raveyard, the (s)tream, the (c)anyon, walk (f)urther inland, or return to (b)ase: ')
                if choice6 == 'm':
                    monument()
                if choice6 == 'g':
                    graveyard()
                if choice6 == 's':    
                    stream()
                if choice6 == 'c':
                    canyon()
                if choice6 == 'f':
                    print('You walk further inland.')
                    inner_island()
                if choice6 == 'b':
                    print('You return to your camp.')
main()            
